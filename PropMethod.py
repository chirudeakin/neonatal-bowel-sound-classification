from python_speech_features import mfcc
import librosa
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.utils import resample
import librosa.display
import seaborn as sns
import time
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from scipy.stats import ttest_ind, mannwhitneyu
from sklearn.metrics import roc_auc_score
from sklearn.metrics import recall_score
import tensorflow as tf
import keras
from keras.utils import np_utils
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Embedding
from keras.layers import LSTM
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import Input, Flatten, Dropout, Activation
from keras.layers import Conv1D, MaxPooling1D, AveragePooling1D, LSTM
from keras.models import Model
from keras.callbacks import ModelCheckpoint
from sklearn.preprocessing import LabelEncoder
from keras.layers import BatchNormalization, Lambda, Bidirectional
from keras.layers import Conv2D, MaxPooling2D
from hmmlearn.hmm import MultinomialHMM
from keras.callbacks import EarlyStopping
import tensorflow as tf
import os

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)

# samples = 0  ## ???#mention the samples
# path = 'C:/Users/Sam/Desktop/OneDrive/Monash @Dr. Faezeh Marzbanrad/Workspace/Bowel Sound Classification/Feature Engineering Method/'
path = 'C:/Users/Chiru/PycharmProjects/Neonatal/'


def read_annotation(file):
    df = pd.read_table(file, sep='\t', header=None)
    ann = {}
    segs_0 = []
    segs_1 = []

    # extract segments with peristalticSound
    for idx, label in enumerate(df[8]):
        if label.strip().find('peristalticSound') != -1:
            segs_1.append([df.loc[idx, 3], df.loc[idx, 5]])
        else:
            continue
    ann[1] = segs_1

    # extract segments without peristalticSound
    last = 0.00
    for segment in segs_1:
        if segment[0] - last > 3:
            segs_0.append([last, segment[0]])
        last = segment[1]
    ann[0] = segs_0

    return ann


data = []
features = []
labels = []
subjects = []
segLen = 6
overlap = 0.1

# Retrieve file name from the list

# Get data

samples = os.listdir(path + '/data')

s = []

for f in samples:
    k = f.split('.')
    print(str(k[0]) + '\n')
    s.append(k[0])

samples = list(set(s))

for sample in samples:
    annotation = read_annotation(path + 'data/' + sample + '.txt')
    wave, sr = librosa.load(path + 'data/' + sample + '.wav', sr=None)
    for label in range(2):
        for values in annotation[label]:
            if values[1] - values[0] < segLen:
                continue
            else:
                offset = 0
                while True:
                    start = int((values[0] + offset) * sr)
                    end = int((values[0] + offset + segLen) * sr)
                    if values[0] + offset + segLen > values[1]:
                        break
                    seg = wave[start: end]
                    labels.append(label)
                    subjects.append(sample)
                    data.append(seg)
                    offset += overlap
                    features.append(np.mean(mfcc(seg, sr, numcep=24), axis=0))

pd_features = pd.DataFrame(features)
# pd_features.to_csv('segments.csv')

pd_features.head()


# Residual based DLL
# def residual_block(filters, conv_num=3, activation="relu"):
#     # Shortcut
#     input_layer = keras.layers.Input((24, 1))
#     s = keras.layers.Conv1D(filters, 1, padding="same")(input_layer)
#     for i in range(conv_num - 1):
#         x = keras.layers.Conv1D(filters, 3, padding="same")(input_layer)
#         x = keras.layers.Activation(activation)(x)
#     x = keras.layers.Conv1D(filters, 3, padding="same")(x)
#     x = keras.layers.Add()([x, s])
#     x = keras.layers.Activation(activation)(x)
#     return keras.layers.MaxPool1D(pool_size=2, strides=2)(x)
#
#
# def build_model(num_classes):
#     # inputs = keras.layers.Input(shape=input_shape, name="input")
#     input_layer = keras.layers.Input((24, 1))
#     x = residual_block(input_layer, 16, 2)
#     x = residual_block(x, 32, 2)
#     x = residual_block(x, 64, 3)
#     x = residual_block(x, 128, 3)
#     x = residual_block(x, 128, 3)
#
#     x = keras.layers.AveragePooling1D(pool_size=3, strides=3)(x)
#     x = keras.layers.Flatten()(x)
#     x = keras.layers.Dense(256, activation="relu")(x)
#     x = keras.layers.Dense(128, activation="relu")(x)
#
#     outputs = keras.layers.Dense(num_classes, activation="softmax", name="output")(x)
#
#     return keras.models.Model(inputs=input_layer, outputs=outputs)
#
#
# model = build_model(2)

def mcnn():
    input_layer = keras.layers.Input((24, 1))
    x = Conv1D(256, 2, padding='same')(input_layer)
    x = Activation('relu')(x)
    x = Conv1D(128, 4, padding='same')(x)
    x = Activation('relu')(x)
    x = Dropout(0.1)(x)
    x = MaxPooling1D(pool_size=(2))(x)
    x = Conv1D(128, 4, padding='same', )(x)
    x = Activation('relu')(x)
    x = Conv1D(128, 8, padding='same')(x)
    x = Activation('relu')(x)

    y = Conv1D(256, 4, padding='same')(input_layer)
    y = Activation('relu')(y)
    y = Conv1D(128, 4, padding='same')(y)
    y = Activation('relu')(y)
    y = Dropout(0.1)(y)
    y = MaxPooling1D(pool_size=(2))(y)
    y = Conv1D(128, 4, padding='same')(y)
    y = Activation('relu')(y)
    # y = Conv1D(128, 10, padding='same')(y)
    y = Conv1D(128, 8, padding='same')(y)
    y = Activation('relu')(y)

    z = Conv1D(256, 8, padding='same')(input_layer)
    z = Activation('relu')(z)
    z = Conv1D(128, 4, padding='same')(z)
    z = Activation('relu')(z)
    z = Dropout(0.1)(z)
    z = MaxPooling1D(pool_size=(2))(z)
    z = Conv1D(128, 4, padding='same')(z)
    z = Activation('relu')(z)
    z = Conv1D(128, 8, padding='same')(z)
    z = Activation('relu')(z)

    t = Conv1D(256, 16, padding='same')(input_layer)
    t = Activation('relu')(t)
    t = Conv1D(128, 4, padding='same')(t)
    t = Activation('relu')(t)
    t = Dropout(0.1)(t)
    t = MaxPooling1D(pool_size=(2))(t)
    t = Conv1D(128, 4, padding='same')(t)
    t = Activation('relu')(t)
    t = Conv1D(128, 8, padding='same')(t)
    t = Activation('relu')(t)

    add_layer = keras.layers.add([x, y, z, t])
    output_layer = Flatten()(add_layer)
    output_layer = Dense(16)(output_layer)
    output_layer = Dense(2)(output_layer)
    output_layer = Activation('softmax')(output_layer)
    model = keras.models.Model(inputs=input_layer, outputs=output_layer)
    # opt = keras.optimizers.rmsprop(lr=0.00001, decay=1e-6)
    opt = keras.optimizers.rmsprop(lr=0.00001, decay=1e-6)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
    return model


def mcnn_original():
    input_layer = keras.layers.Input((24, 1))
    x = Conv1D(256, 2, padding='same')(input_layer)
    x = Activation('relu')(x)
    x = Conv1D(128, 4, padding='same')(x)
    x = Activation('relu')(x)
    x = Dropout(0.1)(x)
    x = MaxPooling1D(pool_size=(2))(x)
    x = Conv1D(128, 4, padding='same', )(x)
    x = Activation('relu')(x)
    x = Conv1D(128, 8, padding='same')(x)
    x = Activation('relu')(x)

    y = Conv1D(256, 4, padding='same')(input_layer)
    y = Activation('relu')(y)
    y = Conv1D(128, 4, padding='same')(y)
    y = Activation('relu')(y)
    y = Dropout(0.1)(y)
    y = MaxPooling1D(pool_size=(2))(y)
    y = Conv1D(128, 4, padding='same')(y)
    y = Activation('relu')(y)
    y = Conv1D(128, 10, padding='same')(y)
    # y = Conv1D(128, 8, padding='same')(y)
    y = Activation('relu')(y)

    z = Conv1D(256, 8, padding='same')(input_layer)
    z = Activation('relu')(z)
    z = Conv1D(128, 4, padding='same')(z)
    z = Activation('relu')(z)
    z = Dropout(0.1)(z)
    z = MaxPooling1D(pool_size=(2))(z)
    z = Conv1D(128, 4, padding='same')(z)
    z = Activation('relu')(z)
    z = Conv1D(128, 8, padding='same')(z)
    z = Activation('relu')(z)

    t = Conv1D(256, 16, padding='same')(input_layer)
    t = Activation('relu')(t)
    t = Conv1D(128, 4, padding='same')(t)
    t = Activation('relu')(t)
    t = Dropout(0.1)(t)
    t = MaxPooling1D(pool_size=(2))(t)
    t = Conv1D(128, 4, padding='same')(t)
    t = Activation('relu')(t)
    t = Conv1D(128, 8, padding='same')(t)
    t = Activation('relu')(t)

    add_layer = keras.layers.add([x, y, z, t])
    # add_layer=keras.layers.concatenate([x,y,z,t])
    output_layer = Flatten()(add_layer)
    # output_layer=Dropout(0.3)(output_layer)
    output_layer = Dense(16)(output_layer)
    output_layer = Dense(2)(output_layer)
    output_layer = Activation('softmax')(output_layer)

    model = keras.models.Model(inputs=input_layer, outputs=output_layer)
    opt = keras.optimizers.rmsprop(lr=0.00001, decay=1e-6)
    # opt = keras.optimizers.adam(lr=0.001)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])

    return model


def cnn_lstm():
    input_layer = keras.layers.Input((24, 1))
    x = Conv1D(256, 2, padding='same')(input_layer)
    x = Activation('relu')(x)
    x = Conv1D(128, 4, padding='same')(x)
    x = Activation('relu')(x)
    x = Dropout(0.1)(x)
    x = MaxPooling1D(pool_size=(2))(x)
    lstm1 = LSTM(32)(x)
    output_layer = Dense(2)(lstm1)
    output_layer = Activation('softmax')(output_layer)
    model = keras.models.Model(inputs=input_layer, outputs=output_layer)
    opt = keras.optimizers.adam(lr=0.00001, decay=1e-6)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
    return model


def core_lstm():
    model = Sequential()
    model.add(LSTM(256, input_shape=(24, 1)))
    model.add(Dropout(0.1))
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.1))
    # model.add(Dense(64, activation='relu'))
    model.add(Dense(2, activation='softmax'))
    opt = keras.optimizers.adam(lr=0.001)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
    return model


def uni_channel_mcnn():
    input_layer = keras.layers.Input((24, 1))
    x = Conv1D(256, 2, padding='same')(input_layer)
    x = Activation('relu')(x)
    x = Conv1D(128, 4, padding='same')(x)
    x = Activation('relu')(x)
    x = Dropout(0.1)(x)
    x = MaxPooling1D(pool_size=(2))(x)
    x = Conv1D(128, 4, padding='same', )(x)
    x = Activation('relu')(x)
    x = Conv1D(128, 8, padding='same')(x)
    x = Activation('relu')(x)

    # y = Conv1D(256, 4, padding='same')(input_layer)
    # y = Activation('relu')(y)
    # y = Conv1D(128, 4, padding='same')(y)
    # y = Activation('relu')(y)
    # y = Dropout(0.1)(y)
    # y = MaxPooling1D(pool_size=(2))(y)
    # y = Conv1D(128, 4, padding='same')(y)
    # y = Activation('relu')(y)
    # y = Conv1D(128, 10, padding='same')(y)
    # y = Activation('relu')(y)

    # z = Conv1D(256, 8, padding='same')(input_layer)
    # z = Activation('relu')(z)
    # z = Conv1D(128, 4, padding='same')(z)
    # z = Activation('relu')(z)
    # z = Dropout(0.1)(z)
    # z = MaxPooling1D(pool_size=(2))(z)
    # z = Conv1D(128, 4, padding='same')(z)
    # z = Activation('relu')(z)
    # z = Conv1D(128, 8, padding='same')(z)
    # z = Activation('relu')(z)

    # t = Conv1D(256, 16, padding='same')(input_layer)
    # t = Activation('relu')(t)
    # t = Conv1D(128, 4, padding='same')(t)
    # t = Activation('relu')(t)
    # t = Dropout(0.1)(t)
    # t = MaxPooling1D(pool_size=(2))(t)
    # t = Conv1D(128, 4, padding='same')(t)
    # t = Activation('relu')(t)
    # t = Conv1D(128, 8, padding='same')(t)
    # t = Activation('relu')(t)

    # add_layer = keras.layers.add([x, y, z, t])

    output_layer = Flatten()(x)
    output_layer = Dense(16)(output_layer)
    output_layer = Dense(2)(output_layer)
    output_layer = Activation('softmax')(output_layer)

    model = keras.models.Model(inputs=input_layer, outputs=output_layer)
    opt = keras.optimizers.rmsprop(lr=0.00001, decay=1e-6)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])

    return model


# Train the model and save it in the directory for reproducibility

def plot_history(history):
    print(history.history.keys())
    #  "Accuracy"
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Validation'], loc='upper left')
    plt.show()
    # "Loss"
    # plt.plot(history.history['loss'])
    # plt.plot(history.history['val_loss'])
    # plt.title('Model loss')
    # plt.ylabel('Loss')
    # plt.xlabel('Epoch')
    # plt.legend(['Train', 'Validation'], loc='upper left')
    # plt.show()


k = 0
for test_sub in samples:
    print('Test_subject: ', test_sub)
    print(str(k) + '\n')
    k = k + 1
    train = []
    train_true = []
    train_lb = []
    test = []
    test_lb = []

    for idx, sub in enumerate(subjects):
        if sub == test_sub:
            test.append(features[idx])
            test_lb.append(labels[idx])
        else:
            # train.append(features[idx])
            # train_lb.append(labels[idx])
            if labels[idx] == 1:
                train_true.append(features[idx])
            else:
                train.append(features[idx])
                train_lb.append(labels[idx])
    train += resample(train_true, replace=False, n_samples=len(train_lb))  # Down sampling
    train_lb += [1 for i in train_lb]

    test_lb.append(0)
    test_lb.append(1)
    x_train = np.array(train)
    y_train = np.array(train_lb)
    x_test = np.array(test)
    y_test = np.array(test_lb)

    lb = LabelEncoder()

    y_train = np_utils.to_categorical(lb.fit_transform(y_train))
    y_test = np_utils.to_categorical(lb.fit_transform(y_test))[:-2]

    x_traincnn = np.expand_dims(x_train, axis=2)
    x_testcnn = np.expand_dims(x_test, axis=2)

    model = mcnn()
    # es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=200)
    # mc = ModelCheckpoint(path + 'pre_trained_models/' + test_sub + '.h5',
    # monitor='val_accuracy', mode='max', verbose=1, save_best_only=True)
    # Train model here with training dataset and use it for training and testing. #7:3, batch=32--marginal increment
    history = model.fit(x_traincnn, y_train, nb_epoch=200, batch_size=16, validation_split=0.3,
                        verbose=False)
    # history = model.fit(x_traincnn, y_train, nb_epoch=40, batch_size=16, validation_data=(x_testcnn,y_test),
    #                     verbose=False,callbacks=[es,mc])
    # plot_history(history)
    # break;
    model.save_weights(path + 'pre_trained_models/' + test_sub + '.h5')

# Load models and evaluate the proposed methods
result = np.array([[0, 0], [0, 0]])
groundTruth = []
overallPrediction = []
probaPredictions = {}
for test_sub in samples:
    print('Test_subject: ', test_sub)
    train = []
    train_true = []
    train_lb = []
    test = []
    test_lb = []

    for idx, sub in enumerate(subjects):
        if sub == test_sub:
            test.append(features[idx])
            test_lb.append(labels[idx])
        else:
            # train.append(features[idx])
            # train_lb.append(labels[idx])
            if labels[idx] == 1:
                train_true.append(features[idx])
            else:
                train.append(features[idx])
                train_lb.append(labels[idx])
    train += resample(train_true, replace=False, n_samples=len(train_lb))  # Downsampling
    train_lb += [1 for i in train_lb]

    test_lb.append(0)
    test_lb.append(1)
    x_train = np.array(train)
    y_train = np.array(train_lb)
    x_test = np.array(test)
    y_test = np.array(test_lb)

    lb = LabelEncoder()

    y_train = np_utils.to_categorical(lb.fit_transform(y_train))
    y_test = np_utils.to_categorical(lb.fit_transform(y_test))[:-2]

    x_traincnn = np.expand_dims(x_train, axis=2)
    x_testcnn = np.expand_dims(x_test, axis=2)

    # save_path = 'trained model/'
    model = mcnn()
    model.load_weights(path + 'pre_trained_models/' + test_sub + '.h5')
    #
    # opt = keras.optimizers.rmsprop(lr=0.00001, decay=1e-6)
    # model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy']

    prediction = model.predict(x_testcnn)
    probaPredictions[test_sub] = prediction.copy()
    overallPrediction += list(prediction.argmax(axis=1))
    groundTruth += list(y_test.argmax(axis=1))
    cm = confusion_matrix(
        y_test.argmax(axis=1), prediction.argmax(axis=1), labels=[0, 1]
    )
    result += cm

print('Final result:', result)
print('Acc: ', accuracy_score(groundTruth, overallPrediction))
print(classification_report(groundTruth, overallPrediction))

from hsmmlearn.hsmm import GaussianHSMM
from hsmmlearn.hsmm import HSMMModel

from scipy.stats import laplace

from hsmmlearn.emissions import AbstractEmissions, MultinomialEmissions


class LaplaceEmissions(AbstractEmissions):
    dtype = np.float64

    def __init__(self, means, scales):
        self.means = means
        self.scales = scales

    def likelihood(self, obs):
        obs = np.squeeze(obs)
        return laplace.pdf(obs,
                           loc=self.means[:, np.newaxis],
                           scale=self.scales[:, np.newaxis])

    def sample_for_state(self, state, size=None):
        return laplace.rvs(self.means[state], self.scales[state], size)


def hsmm_get_transition_matrix(test_sub, sub_labels, samples):
    tm_cnt = np.zeros((2, 2), dtype=float)
    for temp_sub in samples:
        if temp_sub == test_sub:
            continue

        sub_seq = list(sub_labels[sub_labels[0] == temp_sub][1])
        for idx in range(len(sub_seq) - 1):
            if sub_seq[idx] == sub_seq[idx + 1]:
                continue
            tm_cnt[sub_seq[idx], sub_seq[idx + 1]] += 1
    tm = np.zeros((2, 2), dtype=float)
    for i in range(2):
        for j in range(2):
            tm[i, j] = tm_cnt[i, j] / np.sum(tm_cnt[i])
    return tm


def hsmm_get_emission_matrix(train_cm):
    em = np.zeros((2, 2), dtype=float)
    for i in range(2):
        for j in range(2):
            em[i, j] = train_cm[j, i] / np.sum(train_cm[j])
    return em


def hsmm_get_duration(test_sub, sub_labels, samples):
    dr_cnt = np.zeros((2, 1000), dtype=float)
    for temp_sub in samples:
        if temp_sub == test_sub:
            continue

        sub_seq = list(sub_labels[sub_labels[0] == temp_sub][1])
        idx = 0
        pre_state = 0
        cnt = 0
        while idx < len(sub_seq):
            if sub_seq[idx] == pre_state:
                cnt += 1
            else:
                if cnt != 0:
                    dr_cnt[pre_state, cnt - 1] += 1
                pre_state = sub_seq[idx]
                cnt = 1
            idx += 1
        if cnt != 0:
            dr_cnt[pre_state, cnt - 1] += 1
    dr = np.zeros((2, 1000), dtype=float)
    for i in range(2):
        dr[i] = dr_cnt[i] / np.sum(dr_cnt[i])
    return dr


def new_hsmm_get_duration(test_sub, sub_labels, samples):
    test_len = 600
    dr_cnt = np.ones((2, test_len), dtype=float)
    for temp_sub in samples:
        if temp_sub == test_sub:
            continue

        sub_seq = list(sub_labels[sub_labels[0] == temp_sub][1])
        idx = 0
        pre_state = 0
        cnt = 0
        while idx < len(sub_seq):
            if sub_seq[idx] == pre_state:
                cnt += 1
            else:
                if cnt != 0:
                    if cnt > test_len:
                        cnt = test_len
                    dr_cnt[pre_state, cnt - 1] += 100
                pre_state = sub_seq[idx]
                cnt = 1
            idx += 1
        if cnt != 0:
            if cnt > test_len:
                cnt = test_len
            dr_cnt[pre_state, cnt - 1] += 100
    dr = np.zeros((2, test_len), dtype=float)
    for i in range(2):
        dr[i] = dr_cnt[i] / np.sum(dr_cnt[i])
    return dr


def hsmm_get_init_probability(samples, test_sub):
    ini_prob = []
    cnt0 = 0
    cnt1 = 0
    for test_sub in samples:
        test_lb = []
        for idx, sub in enumerate(subjects):
            if sub == test_sub:
                test_lb.append(labels[idx])
        ini_prob.append(test_lb[0])
        if test_lb[0] == 0:
            cnt0 += 1
        else:
            cnt1 += 1
    return np.array([cnt0 / 49, cnt1 / 49])


overallPrediction = {}
newOverallPrediction = {}
for s in np.linspace(0.1, 5.0, num=50):
    overallPrediction[str(s)] = []
    newOverallPrediction[str(s)] = []
    for test_sub in samples:
        prediction = probaPredictions[test_sub]
        overallPrediction[str(s)] += list(prediction.argmax(axis=1))
        sub_labels = pd.concat([pd.Series(subjects), pd.Series(labels)], axis=1)

        tm = np.array([
            [0.001, 0.999],
            [0.999, 0.001]
        ])

        means = np.array([0.0, 1.0])
        scales = np.array([s, s])
        # scales = np.array([1.5, 1.5])
        dm = new_hsmm_get_duration(test_sub, sub_labels, samples)
        # dm = hsmm_get_duration(test_sub, sub_labels, samples)
        st = hsmm_get_init_probability(samples, test_sub)

        # h1 = GaussianHSMM(means, scales, dm, tm, st)
        h1 = HSMMModel(LaplaceEmissions(means, scales), dm, tm, st)
        # h1 = HSMMModel(MultinomialEmissions(hsmm_get_emission_matrix(train_cm)), dm, tm, st)

        # temp_observations = prediction.argmax(axis=1).reshape(-1,)
        temp_observations = prediction[:, 1].reshape(-1, )
        # observations = np.zeros_like(temp_observations, dtype='float64')
        observations = np.zeros_like(temp_observations, dtype='float64')
        for idx, sta in enumerate(temp_observations):
            observations[idx] = sta

        new_prediction = h1.decode(observations)

        newOverallPrediction[str(s)] += list(new_prediction)

x = []
y1 = []
y2 = []

for s in np.linspace(0.1, 5.0, num=50):
    x.append(s)
    y1.append(accuracy_score(groundTruth, newOverallPrediction[str(s)]))
    y2.append(roc_auc_score(groundTruth, newOverallPrediction[str(s)]))
    print(s)
    print(roc_auc_score(groundTruth, newOverallPrediction[str(s)]))
    print(accuracy_score(groundTruth, newOverallPrediction[str(s)]))
    print()

# s = 3.6
s = 4.1
print('Before Refinement: ')
print(classification_report(groundTruth, overallPrediction[str(s)], digits=4))
print('After Refinement: ')
print(classification_report(groundTruth, newOverallPrediction[str(s)], digits=4))
print()
print('Before Refinement: ')
print('ACC: ', accuracy_score(groundTruth, overallPrediction[str(s)]))
print('After Refinement: ')
print('ACC: ', accuracy_score(groundTruth, newOverallPrediction[str(s)]))
print()
print('Before Refinement: ')
print('AUC: ', roc_auc_score(groundTruth, overallPrediction[str(s)]))
print('After Refinement: ')
print('AUC: ', roc_auc_score(groundTruth, newOverallPrediction[str(s)]))

a = pd.DataFrame({'x': x, 'y1': y1, 'y2': y2})
import matplotlib.pyplot as plt
import seaborn as sns

fig = plt.figure(dpi=100, figsize=(10, 5))
plt.rc('font', family='STFangsong')
sns.set(style="darkgrid")

plt.ylim(0.8, 0.9)

sns.scatterplot(x="x",
                y="y1",
                data=a, label='ACC')
sns.lineplot(x="x",
             y="y1",
             data=a)

sns.scatterplot(x="x",
                y="y2",
                data=a, label='AUC')
sns.lineplot(x="x",
             y="y2",
             data=a)

plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.xlabel('Standard deviation', fontdict={'size': 12})
plt.ylabel('Value', fontdict={'size': 15})
plt.legend(loc='upper left', labels=['ACC', 'AUC'], fontsize=12)
plt.savefig('hsmm parameters.png')

result = np.array([[0, 0], [0, 0]])
groundTruth = []
conventional_overallPrediction = []
conventional_newOverallPrediction = []

for test_sub in samples:
    print('Test_subject: ', test_sub)
    train = []
    train_true = []
    train_lb = []
    test = []
    test_lb = []

    for idx, sub in enumerate(subjects):
        if sub == test_sub:
            test.append(features[idx])
            test_lb.append(labels[idx])
        else:
            # train.append(features[idx])
            # train_lb.append(num_labels[idx])
            if labels[idx] == 1:
                train_true.append(features[idx])
            else:
                train.append(features[idx])
                train_lb.append(labels[idx])
    train += resample(train_true, replace=False, n_samples=len(train_lb))
    train_lb += [1 for i in train_lb]

    test_lb.append(0)
    test_lb.append(1)
    x_train = np.array(train)
    y_train = np.array(train_lb)
    x_test = np.array(test)
    y_test = np.array(test_lb)

    lb = LabelEncoder()

    y_train = np_utils.to_categorical(lb.fit_transform(y_train))
    y_test = np_utils.to_categorical(lb.fit_transform(y_test))[:-2]

    x_traincnn = np.expand_dims(x_train, axis=-1)
    x_testcnn = np.expand_dims(x_test, axis=-1)

    # save_path = 'trained_model/'
    model = mcnn()
    model.load_weights(path + 'pre_trained_models/' + test_sub + '.h5')
    prediction = model.predict(x_testcnn)
    conventional_overallPrediction += list(prediction.argmax(axis=1))
    groundTruth += list(y_test.argmax(axis=1))

    sub_labels = pd.concat([pd.Series(subjects), pd.Series(labels)], axis=1)
    train_cm = confusion_matrix(
        y_train.argmax(axis=1), model.predict(x_traincnn).argmax(axis=1), labels=[0, 1]
    )

    tm = np.array([
        [0.001, 0.999],
        [0.999, 0.001]
    ])

    means = np.array([0.0, 1.0])
    scales = np.array([1, 1])
    # scales = np.array([1, 1])
    dm = new_hsmm_get_duration(test_sub, sub_labels, samples)
    st = np.array([0.5, 0.5])

    h1 = HSMMModel(MultinomialEmissions(hsmm_get_emission_matrix(train_cm).T), dm, tm, st)

    temp_observations = prediction.argmax(axis=1).reshape(-1, )

    # observations = np.zeros_like(temp_observations, dtype='float64')
    observations = np.zeros_like(temp_observations, dtype='int')
    for idx, sta in enumerate(temp_observations):
        observations[idx] = sta

    new_prediction = h1.decode(observations)

    conventional_newOverallPrediction += list(new_prediction)

print(classification_report(groundTruth, conventional_overallPrediction, digits=4))
print(classification_report(groundTruth, conventional_newOverallPrediction, digits=4))
print(accuracy_score(groundTruth, conventional_overallPrediction))
print(accuracy_score(groundTruth, conventional_newOverallPrediction))

print('Before Refinement: ')
print(classification_report(groundTruth, conventional_overallPrediction, digits=4))
print('After Refinement: ')
print(classification_report(groundTruth, conventional_newOverallPrediction, digits=4))
print()
print('Before Refinement: ')
print('ACC: ', accuracy_score(groundTruth, conventional_overallPrediction))
print('After Refinement: ')
print('ACC: ', accuracy_score(groundTruth, conventional_newOverallPrediction))
print()
print('Before Refinement: ')
print('AUC: ', roc_auc_score(groundTruth, conventional_overallPrediction))
print('After Refinement: ')
print('AUC: ', roc_auc_score(groundTruth, conventional_newOverallPrediction))
